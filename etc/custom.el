;;; custom.el --- Programmatically set custom variables
;;;
;;; Commentary:
;;; Auto-generated custom variables set by Emacs
;;;
;;; Code:

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(web-mode god-mode mode-line-bell modus-themes rainbow-delimiters php-mode projectile ripgrep magit lsp-ivy lsp-ui lsp-mode flycheck flyspell-correct-avy-menu flyspell-correct which-key ws-butler avy helpful ivy-rich counsel exec-path-from-shell auto-package-update use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'custom.el)
;;; custom.el ends here
